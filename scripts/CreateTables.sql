DROP TABLE IF EXISTS Pant;
DROP TABLE IF EXISTS Brand;

CREATE TABLE Brand (
    brandId INT AUTO_INCREMENT,
    name VARCHAR(190) NOT NULL UNIQUE,
    PRIMARY KEY (brandId)
) ENGINE=INNODB;

CREATE TABLE Pant (
    pantId INT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    brandId INT,
    rise FLOAT,
    thigh FLOAT,
    knee FLOAT,
    ankle FLOAT,
    image VARCHAR(255),
    PRIMARY KEY (pantId),
    FOREIGN KEY (brandId) REFERENCES Brand(brandId)
) ENGINE=INNODB;
