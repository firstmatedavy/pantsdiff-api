<?php
require_once('vendor/autoload.php');

use App\Database\DbFactory;

$dbFactory = new DbFactory();
$db = $dbFactory->getDb();

$db->prepare('SET foreign_key_checks = 0')->execute([]);
$db->prepare('TRUNCATE TABLE Pant')->execute([]);
$db->prepare('TRUNCATE TABLE Brand')->execute([]);
$db->prepare('SET foreign_key_checks = 1')->execute([]);


$brandQuery = 'INSERT INTO Brand (name) VALUES (:brand)';
$brandStatement = $db->prepare($brandQuery);
$brandParamsList = [
    [':brand' => "Levi's"],
    [':brand' => 'Unbranded'],
    [':brand' => 'Naked and Famous']
];
foreach ($brandParamsList as $brandParams) {
    $brandStatement->execute($brandParams);
}


$pantsJson = file_get_contents(__DIR__ . '/../data/pants.json');
$pants = json_decode($pantsJson, true);

$query = 'INSERT INTO Pant (name, brandId, rise, thigh, knee, ankle, image)
    VALUES (:name, (SELECT brandId FROM Brand WHERE name=:brand), :rise, :thigh, :knee, :ankle, :image)';
$statement = $db->prepare($query);

foreach ($pants as $pant) {
    $params = [
        ':name' => $pant['name'],
        ':brand' => $pant['brand'],
        ':rise' => $pant['rise'],
        ':thigh' => $pant['thigh'],
        ':knee' => $pant['knee'],
        ':ankle' => $pant['ankle'],
        ':image' => $pant['image']
    ];
    $statement->execute($params);
}
