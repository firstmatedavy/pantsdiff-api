<?php
namespace App\Service;

use App\Model\PantMeasurements;
use App\Repository\PantsRepository;

class PantSuggestionService
{
    const DEFAULT_FLEXIBILITY = 1;
    const MAX_FLEXIBILITY = 5;
    const MIN_FLEXIBILITY = .25;

    /** @var PantsRepository */
    private $pantsRepository;

    public function __construct(PantsRepository $pantsRepository)
    {
        $this->pantsRepository = $pantsRepository ?: new PantsRepository();
    }

    public function getSuggestions(PantMeasurements $measurements, int $minCount = 2, int $maxCount = 10): array
    {
        $flexibility = self::DEFAULT_FLEXIBILITY;
        $flexibilityHistory = [];
        $hasSucceeded = false;
        while (!$hasSucceeded) {
            $flexibilityHistory[] = $flexibility;
            $results = $this->queryRepository($measurements, $flexibility);
            if (count($results) < $minCount) {
               $flexibility = $this->increaseFlexibility($flexibility);
            } elseif (count($results) > $maxCount) {
                $flexibility = $this->decreaseFlexibility($flexibility);
            } else {
                //todo: log flexibility history
                return $results;
            }
        }
    }

    private function queryRepository(PantMeasurements $measurements, float $flexibility)
    {
        return $this->pantsRepository->getPantsByCriteria($measurements, $flexibility);
    }

    private function decreaseFlexibility(float $prior)
    {
        return (self::MIN_FLEXIBILITY + $prior)/2 + self::MIN_FLEXIBILITY;
    }

    private function increaseFlexibility(float $prior)
    {
        return (self::MAX_FLEXIBILITY - $prior)/2 + ($prior/2);
    }
}
