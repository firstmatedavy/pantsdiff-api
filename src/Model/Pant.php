<?php
namespace App\Model;

use JsonSerializable;

/**
 * Model object for pants
 */
class Pant implements JsonSerializable
{
    /** @var int|null */
    private $pantId;

    /** @var string */
    private $name;

    /** @var string */
    private $brand;

    /** @var PantMeasurements */
    private $measurements;

    /** @var string */
    private $image;

    /**
     * @param string $name
     * @param string $brand
     * @param float $rise
     * @param float $thigh
     * @param float $knee
     * @param float $ankle
     * @param string $image
     * @param int|null $pantId pant ID may be null for pants not yet in the database
     */
    public function __construct(
        string $name,
        string $brand,
        float $rise,
        float $thigh,
        float $knee,
        float $ankle,
        string $image,
        int $pantId = null
    ) {
        $this->name = $name;
        $this->brand = $brand;
        $this->measurements = new PantMeasurements($rise, $thigh, $knee, $ankle);
        $this->image = $image;
        $this->pantId = $pantId;
    }

    /**
     * @return int
     */
    public function getPantId(): int
    {
        return $this->pantId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @return float
     */
    public function getRise(): float
    {
        return $this->measurements->getRise();
    }

    /**
     * @return float
     */
    public function getThigh(): float
    {
        return $this->measurements->getThigh();
    }

    /**
     * @return float
     */
    public function getKnee(): float
    {
        return $this->measurements->getKnee();
    }

    /**
     * @return float
     */
    public function getAnkle(): float
    {
        return $this->measurements->getAnkle();
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * Serializer for JSON_ENCODE
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->pantId,
            'brand' => $this->brand,
            'name' => $this->name,
            'rise' => $this->getRise(),
            'thigh' => $this->getThigh(),
            'knee' => $this->getKnee(),
            'ankle' => $this->getAnkle(),
            'image' => $this->image
        ];
    }
}
