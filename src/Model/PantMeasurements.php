<?php
namespace App\Model;


class PantMeasurements
{
    /** @var float */
    private $rise;

    /** @var float */
    private $thigh;

    /** @var float */
    private $knee;

    /** @var float */
    private $ankle;

    /**
     * @param float $rise
     * @param float $thigh
     * @param float $knee
     * @param float $ankle
     */
    public function __construct(
        float $rise,
        float $thigh,
        float $knee,
        float $ankle
    ) {
        $this->rise = $rise;
        $this->thigh = $thigh;
        $this->knee = $knee;
        $this->ankle = $ankle;
    }

    /**
     * @return float
     */
    public function getRise(): float
    {
        return $this->rise;
    }

    /**
     * @return float
     */
    public function getThigh(): float
    {
        return $this->thigh;
    }

    /**
     * @return float
     */
    public function getKnee(): float
    {
        return $this->knee;
    }

    /**
     * @return float
     */
    public function getAnkle(): float
    {
        return $this->ankle;
    }
}
