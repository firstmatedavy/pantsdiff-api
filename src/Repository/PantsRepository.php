<?php
namespace App\Repository;

use App\Database\Db;
use App\Database\DbFactory;
use App\Model\Pant;
use App\Model\PantMeasurements;
use App\Serializer\PantSerializer;
use PDO;

/**
 * Repository object for getting Pant data out of the database
 */
class PantsRepository
{
    /** @var PantSerializer */
    private $pantSerializer;

    /** @var Db */
    private $db;

    /**
     * @param PantSerializer|null $pantSerializer
     * @param DbFactory|null $dbFactory
     */
    public function __construct(PantSerializer $pantSerializer = null, DbFactory $dbFactory = null)
    {
        $this->pantSerializer = $pantSerializer ?: new PantSerializer();
        $dbFactory = $dbFactory ?: new DbFactory();
        $this->db = $dbFactory->getDb();
    }

    public function getAllPants(): array
    {
        $query = 'SELECT pantId, Pant.name as name, Brand.name AS brand, rise, thigh, knee, ankle, image
                    FROM Pant JOIN Brand ON Pant.brandId = Brand.brandId
                    LIMIT 100';
        return $this->queryPants($query);
    }

    public function getPant(int $pantId): Pant
    {
        $query = 'SELECT pantId, Pant.name as name, Brand.name AS brand, rise, thigh, knee, ankle, image
            FROM Pant JOIN Brand ON Pant.brandId = Brand.brandId
            WHERE pantId = :pantId
            LIMIT 1';
        $params = [
            ':pantId' => $pantId
        ];
        $result = $this->queryPants($query, $params);
        return $result[0];
    }

    public function searchPants(string $term): array
    {
        $query = 'SELECT pantId, Pant.name as name, Brand.name AS brand, rise, thigh, knee, ankle, image,
                CONCAT(Brand.name, " ", Pant.name) AS brandAndName
            FROM Pant JOIN Brand ON Pant.brandId = Brand.brandId
            HAVING brandAndName LIKE :term
            LIMIT 100';
        $params = [
            ':term' => "%$term%"
        ];
        return $this->queryPants($query, $params);
    }

    public function getPantsByCriteria(PantMeasurements $measurements, float $flexibility): array
    {
        $query = 'SELECT pantId, Pant.name as name, Brand.name AS brand, rise, thigh, knee, ankle, image
            FROM Pant JOIN Brand ON Pant.brandId = Brand.brandId
            WHERE
                rise < :maxRise && rise > :minRise &&
                thigh < :maxThigh && thigh > :minThigh &&
                knee < :maxKnee && knee > :minKnee &&
                ankle < :maxAnkle && ankle > :minAnkle
            LIMIT 20';
        $params = [
            ':maxRise' => $measurements->getRise() + $flexibility,
            ':minRise' => $measurements->getRise() - $flexibility,
            ':maxThigh' => $measurements->getThigh() + $flexibility,
            ':minThigh' => $measurements->getThigh() - $flexibility,
            ':maxKnee' => $measurements->getKnee() + $flexibility,
            ':minKnee' => $measurements->getKnee() - $flexibility,
            ':maxAnkle' => $measurements->getAnkle() + $flexibility,
            ':minAnkle' => $measurements->getAnkle() - $flexibility
        ];
        return $this->queryPants($query, $params);
    }

    private function queryPants(string $query, array $params = null): array
    {
        $statement = $this->db->prepare($query);

        if (empty($params)) {
            $statement->execute();
        } else {
            $statement->execute($params);
        }

        $rawPants = $statement->fetchAll(PDO::FETCH_ASSOC);
        $pants = [];
        foreach ($rawPants as $pant) {
            $pants[] = $this->pantSerializer->unserialize($pant);
        }
        return $pants;
    }
}
