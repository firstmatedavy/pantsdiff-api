<?php
namespace App\Database;

use PDO;

/**
 * Data access class (PDO wrapper)
 *
 * TODO: Ensure there can only be one instance of Db
 */
class Db extends PDO
{
    const CHARSET = 'utf8mb4';
}
