<?php
namespace App\Database;

use PDO;

/**
 * Factory class to get the Db object
 * (This helps database-using classes remain unit testable.)
 */
class DbFactory
{
    /**
     * @return Db
     */
    public function getDb(): Db
    {
        $config = $this->getConfig();
        $db = $this->initializeDb($config['host'], $config['database'], $config['username'], $config['password']);
        return $db;
    }

    /**
     * @return array with keys 'host', 'database', 'user', 'password'
     */
    private function getConfig(): array
    {
        $configJson = file_get_contents(__DIR__ . '/../../config/dbconfig.json');
        return json_decode($configJson, true);
    }

    /**
     * @param string $host
     * @param string $database
     * @param string $username
     * @param string $password
     * @return Db
     */
    private function initializeDb($host, $database, $username, $password): Db
    {
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        $dsn = "mysql:host=$host;dbname=$database;charset=" . Db::CHARSET;

        $db = new Db($dsn, $username, $password, $options);
        return $db;
    }
}
