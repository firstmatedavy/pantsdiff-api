<?php
namespace App\Serializer;

use App\Model\Pant;

/**
 * Convert Pant objects into arrays and vice versa
 */
class PantSerializer
{
    /**
     * @param array $rawPant serialized array of data for one Pant
     * @return Pant unserialized object
     */
    public function unserialize(array $rawPant): Pant
    {
        return new Pant(
            $rawPant['name'],
            $rawPant['brand'],
            $rawPant['rise'],
            $rawPant['thigh'],
            $rawPant['knee'],
            $rawPant['ankle'],
            $rawPant['image'],
            $rawPant['pantId']
        );
    }

    /**
     * @param Pant $pant object to be serialized
     * @return array serialized array of data from the Pant object
     */
    public function serialize(Pant $pant): array
    {
        return [
            'pantId' => $pant->getPantId(),
            'name' => $pant->getName(),
            'brand' => $pant->getBrand(),
            'rise' => $pant->getRise(),
            'thigh' => $pant->getThigh(),
            'knee' => $pant->getKnee(),
            'ankle' => $pant->getAnkle(),
            'image' => $pant->getImage()
        ];
    }
}
