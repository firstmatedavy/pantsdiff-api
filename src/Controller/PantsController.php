<?php

namespace App\Controller;
use App\Model\PantMeasurements;
use App\Repository\PantsRepository;
use App\Service\PantSuggestionService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PantsController
{
    /** @var PantsRepository */
    private $pantsRepository;

    /** @var PantSuggestionService */
    private $pantSuggestionService;

    /**
     * @param PantsRepository $pantsRepository
     * @param PantSuggestionService $pantSuggestionService
     */
    public function __construct(
        PantsRepository $pantsRepository,
        PantSuggestionService $pantSuggestionService
    ) {
        $this->pantsRepository = $pantsRepository;
        $this->pantSuggestionService = $pantSuggestionService;
    }

    /**
     * @Route("/pants/getAll")
     */
    public function getAll()
    {
        $allPants = $this->pantsRepository->getAllPants();

        return new Response(
            json_encode($allPants),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/json',
                'Access-Control-Allow-Origin' => '*'
            ]
        );
    }

    /**
     * @Route("/pants/get/{id}")
     * @param int $id
     */
    public function get(int $id)
    {
        $pant = $this->pantsRepository->getPant($id);

        return new Response(
            json_encode($pant),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/json',
                'Access-Control-Allow-Origin' => '*'
            ]
        );
    }

    /**
     * @Route("/pants/search/{term}")
     * @param string|null $term
     */
    public function searchAction(string $term = '')
    {
        $allPants = $this->pantsRepository->searchPants($term);

        return new Response(
            json_encode($allPants),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/json',
                'Access-Control-Allow-Origin' => '*'
            ]
        );
    }

    /**
     * @Route("/pants/suggest/{requirements}")
     * @param Request $request
     */
    public function suggestAction(string $requirements)
    {
        //todo: it should be possible to have no preference about certain attributes
        $requirements = json_decode($requirements, true);
        $measurements = new PantMeasurements(
            $requirements['rise'],
            $requirements['thigh'],
            $requirements['knee'],
            $requirements['ankle']
        );
        $pants = $this->pantSuggestionService->getSuggestions($measurements);

        return new Response(
            json_encode($pants),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/json',
                'Access-Control-Allow-Origin' => '*'
            ]
        );
    }
}
